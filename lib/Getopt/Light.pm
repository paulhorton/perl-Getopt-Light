#  Author: Paul Horton
#  Copyright (C) 2022, Paul Horton.
#  License:  The Artistic License 2.0 (GPL Compatible)

#  Coding Notes:
#  Use croak for errors from the calling program,
#  but die for erroneous command line input.
package Getopt::Light;


=pod

=head1 NAME

Getopt::Light - a simple ARGV parsing module supporting sane bundling.

=cut


require Exporter;

@ISA=  (Exporter);
@EXPORT=  qw(grabFromARGV);

our $VERSION=  'v0.0.0';
use Carp;
use Data::Lock qw(dlock);  #For internal consistency checks.
use List::Util qw(min);
use Readonly;
use v5.28;
use strict;
use warnings;



my %opts;   #Hash used to assign values to options.
my %name1;  #Map names to their primary names.
my %spec;   #Map primary name to its specifier.
my $checkP; #Check opt values for validity?


#──────────────────────────────  Documented functions  ──────────────────────────────
sub grabFromARGV      {  $checkP= undef;  goto &_grabFromARGV  }
sub grabFromARGV_check{  $checkP= 't';    goto &_grabFromARGV  }


sub opt_name1{   $name1{$_[0]}  //   croak  "No option name found for putative alias '$_[0]'"   }

sub opt_spec{   $spec{opt_name1 $_[0]}   }



#──────────────────────────────  Internal functions  ──────────────────────────────

sub _grabFromARGV{
    my @spec=  @_;
    my $usage=  'Usage:  setopt( spec,... )';
    @_   or   croak  "setopt called with no args\n$usage";

    %opts= %name1= %spec= ();  #Clear all state.
    parse_spec( @spec );
    parse_ARGV();
    return %opts;
}



sub die_prog_error{   confess  'Programming error'  }


sub spec_names{
    wantarray   or   die_prog_error;
    my $specNames=  $_[0]=~ s/=.*\z//r;
    return  split /[|]/, $specNames;
}




Readonly::Scalar  my $char_OK_RX  =>   qr/[^|=\s]/;
Readonly::Scalar  my $char1_OK_RX =>   qr/[^-+0-9.,=|]/;


sub parse_spec{

    my $nameRX=        qr /$char1_OK_RX  $char_OK_RX*/x;

    for (@_){
        next    if  /^\s*#/;
        my @spec=  split /^/;
        for (@spec){
            my ($spec)=  /\A \s* (\S+)/x   or   next;
            my ($name1, @alias)=  spec_names $spec;
            $spec{$name1}=  $spec;
            $opts{$name1}=  $spec=~ /=(.*)/?  $1  :  '';

            for my $name ($name1, @alias){
                remember_name( $name );
                $name=~/\A $nameRX \z/x   or   croak  "Malformed option name '$name'";
                exists $name1{$name}   and   croak  "Option name '$name' alias of both '$name1{name}' and '$name1'";
                $name1{$name}=  $name1;
            }
        }
    }
}


dlock my $H;  #To hold head of @ARGV during parsing.  Declared here so subs can use it in error messages.


sub die_invalid_arg{  die  "Invalid option argument '$H'"  }


sub nameP{  exists $name1{ $_[0] }  }

sub name1{
    my ($name)=  @_;

    return $name1{$name}    if  exists $name1{$name};

    my $msg=  "Unknown option name '$name', in option argument '$H'\n";
    $msg.= similar_names_msg( $name );
    die $msg;
}


#  Is NAME a bridging option?
# sub bridgeP{    $spec{ name1($_[0]) }=~  /=/   }
sub bridgeP{
    my ($name)= @_;
    my $name1=  name1($_[0]);
    $spec{ $name1 }=~  /=/
}


#  Assign VALUE to primary name of ALIAS.
sub assign_opt_value{   $opts{ name1($_[0]) }=  $_[1]   }



#  Get implicit value of option $name.  For when no value is given explicitly.
sub implicit_value{
    my ($name)=  @_;

    bridgeP $name   or   return 1;

    my $negNumArgRX=   qr /\A-  (?: [0-9.,]+ )  |  (?: [0-9.,]+ [eE] [0-9]+ )  \z/x;
    
    if(  @ARGV  ){
        my $nxt=  shift @ARGV;
        $nxt=~ /\A[^-]/   or   $nxt=~ /$negNumArgRX/
          or   die  "option arg '$H' requires a value, but the following arg is '$nxt'\n";
        return $nxt;
    }
    die  "option arg '$H' requires a value, but found no argument after it\n";
}
    


sub parse_ARGV{

    my $longNameRX=    qr /$char1_OK_RX  $char_OK_RX+/x;

    while(  @ARGV  ){
        my ($name, $val);

        Data::Lock::dunlock $H;   dlock $H= shift @ARGV;

        last    if  $H eq '--'   or   $H=~ /\A[^-]/   or   $H=~ /\A-[0-9.,]/;
        die   "Invalid argument '$H'"    if $H=~ /\A-+=/;
        die   "Invalid argument '$H'"    if $H=~ /\A---/;

        if (  $H=~ /\A--/  ) {
            unless(  ($name, $val)=  $H=~ /\A-- ($longNameRX) = (.*)  \z/x  ){
                ($name)=  $H=~ /\A-- ($longNameRX)  \z/x
                  or   die_invalid_arg;
                $val=  implicit_value $name;
            }
            assign_opt_value $name, $val;
            next;
        }


        $H=~ /\A-[^-]/   or   die_prog_error;


        my ($flags, $rest)=  $H=~ /\A-  ($char1_OK_RX+)  (.*)  \z/x
            or   die_invalid_arg;

        my $nameish=  $flags;#  Remember for error message.

        #─────  Handle any name bundling in $flags  ─────
        my $flagFin=  chop $flags;
        my @bundledFlags=   split //, $flags;
        for  my $flag  (@bundledFlags){
            if(  nameP($flag)  ){
                assign_opt_value( $flag,  bridgeP($flag)
                                  ?  1
                                  :  implicit_value $flag
                                );
            }else{
                my $msg=  nameP($flag)
                    ?  "Bundled option '$flag', from argument '$H', requires a value.\n"
                    :  "Unknown option '$flag'\n";
                $msg.=  similar_names_msg( $nameish );
                die $msg;
            }
        }

        #─────  Handle and value or value bundling in $rest  ─────
        my $sep;
        ( $sep, $val )=  $rest=~ /\A  (=?) (.*)  \z/x;

        # length $sep   and   $val=~ /$char1_OK_RX/   and   die  "Invalid bundled value '$val', from argument '$H'";
        # length $val   or   $val= implicit_value $flagFin;
        if(  length $sep  ){
            $val=~ /$char1_OK_RX/   and   die  "Invalid bundled value '$val', from argument '$H'";
        }elsif(  !length $val  ){
            $val=  implicit_value $flagFin;
        }

        assign_opt_value $flagFin, $val;
    }
}



#──────────────────────────────  Diagnostics  ──────────────────────────────

sub similar_names_msg{
    my @similarName=  similar_names( $_[0] );
    my @simSpec=   map  { $spec{name1 $_} }  @similarName;

    @simSpec   or   return '';
    
    my $rt=  "Perhaps:\n";
    for  my $spec  (@simSpec){
        $spec=~ s/( \A | [|] )   ( [^|] [^|]+ )  /$1--$2/xg;
        $spec=~ s/( \A | [|] )   ( [^-|]      )  /$1-$2/xg;
        $rt.=  "    $spec\n";
    }

    return $rt;
}


my @name;   #Hold all option names.

sub remember_name{  push @name, $_[0]  }


#  Return list of labels in @name similar to $query, or empty list if none found.
#  Adapted from code in perltab (same author, Paul Horton)
sub similar_names{
    my ($query)=  @_;

    my @similar;
    
    if(  3 > length $query  ){
        @similar=  grep  {fc $query  eq  fc $_}  @name;
        if(  2 == length $query  ){
            my $rev=  reverse scalar $query;
            push @similar, $rev    if  grep {$rev eq $_}  @name;
        }
        return @similar;
    }
        
    my %dist =  map  {$_ => editDistance($query, $_)}  @name;
    my $minDist=  min values %dist;

    @similar=  grep  {$dist{$_} == $minDist}  @name;

    grep  {$minDist < 0.5* min  map {length}  $query, $_}   @similar;
}


#  Return a kind of edit distance of $s1, $s2
#  Character mismatches differing only in case, such as ('a', 'A'), are penalized lightly.
#  Uses memory proportional to the length of the shorter string.
sub editDistance{
    @_ == 2   or   die_prog_error;
    my( $s1, $s2 )=  sort {length $b <=> length $a}  @_;
    my( $l1, $l2 )=  map {length} ($s1, $s2);
        
    my @T=(   [0..$l2],
              []
          );    #Dynamic programming table;

    my( $cur, $prv )  =  (0, 1);
    for my $i (1..$l1) {
        $prv= $cur;  $cur= 1-$cur; #  $i % 2
        my $c1=  substr $s1, $i-1, 1;
        $T [$cur] [0]=  $i; #Cost of skipping length $i prefix of string $s1.
        for my $j (1..$l2) {
            my $c2=  substr $s2, $j-1, 1;
            my $d=  $c1  eq     $c2?   0
              :  fc $c1  eq  fc $c2?   0.1
              :                        1;
            $T[$cur][$j]  =  min(  $d +  $T [$prv] [$j-1],
                                   1  +  $T [$prv] [$j  ],
                                   1  +  $T [$cur] [$j-1]);
        }
    }
    return  $T [$cur] [$l2];
}#sub editDistance



1;
